# REQUIRED
* python
* pillow package (pip install pillow)
* yaml package (pip install yaml)

# HOW TO USE
* copy ไฟล์ /config/config.yaml.example ไปเป็น /config/config.yaml
* แก้ config.yaml
    - checking directory - เซตให้เป็น path ที่เก็บรูปที่ต้องการเช็ค
* ไปที่ /src แล้วสั่งรัน python kino_checker.py
* รอ...
* ชื่อไฟล์ที่ซ้ำ จะอยู่ใน /log.out

# NOTE
* ryzen 5 รันได้ประมาณ 400 รูป / sec
