from PIL import Image
import os
import yaml

# Setup
config = yaml.load(open('../config/config.yaml').read())
PIXEL_THRESHOLD = config['pixel_threshold']
BASE_IMAGE = config['base_image']
CHECK_DIR = config['checking_dir']
OUTPUT = config['output']


# Load base image
BASE_IMAGE = Image.open(BASE_IMAGE)
base_pixels = BASE_IMAGE.getdata()


# Prepare output file
output_file = open(OUTPUT, 'w')


# Loop through image
total_duplicate = 0
image_names = os.listdir(CHECK_DIR)
print('Total Checking Image:', len(image_names))
for image_name in image_names:
	image_path = os.path.join(CHECK_DIR, image_name)
	img = Image.open(image_path)

	# not mode l is not duplicate
	if img.size != (200,200) or img.mode != 'L':
		continue
	img_pixels = img.getdata()

	diff_pixels_count = 0
	for i in range(len(base_pixels)):
		diff = abs(base_pixels[i] - img_pixels[i])
		if diff > 50:
			diff_pixels_count = diff_pixels_count + 1
	if diff_pixels_count < PIXEL_THRESHOLD:
		output_file.writelines(image_name + '\n')
		total_duplicate = total_duplicate + 1

output_file.close()
print('Total Duplicate Image:', total_duplicate)
